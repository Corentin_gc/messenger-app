const mongoose = require('./mongoose')

const messageSchema = new mongoose.Schema({
    talk: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'talk'
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    content: {
        type: String,
        required: true,
        maxLength: 355
    }
}, {timestamps: true});


const MessageModel = mongoose.model('message', messageSchema);

module.exports = MessageModel