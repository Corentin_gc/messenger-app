const mongoose = require('./mongoose')

const talkSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'user'
    },
    messages: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'message'
        }
    ],
    members: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'user'
        }
    ]
}, {timestamps: true});


const TalkModel = mongoose.model('talk', talkSchema);

module.exports = TalkModel