# Project to create a chat API

## Description 

The aim of this project is to create a Chat API.

It is coded with Express, Mongoose


## Installation 

1. Run ```npm install``` to install all dependencies


2. The postman export : ```Module Back-end.postman_collection.json``` is in directory


3. To lunch the project run ```npm start```

## Features

* Create a user
* Get all users
* Get user by id
* Get connected user
* Login
* Logout
* Create a talk
* Add member to a talk
* Get all talks
* Get talk by id
* Send a message
* Find all messages (of user connected)
* Find all messages by talk (of user connected)

## All routes

1. User

    | Description        | Route          | Method |
    |--------------------|----------------|--------|
    | Create a user      | /api/users/new | POST   |
    | Get all users      | /api/users/all | GET    |
    | Get user by id     | /api/users/:id | GET    |
    | Get connected user | /api/users/me  | GET    |

2. Auth

   | Description | Route            | Method |
   |-------------|------------------|--------|
   | Login       | /api/auth/login  | POST   |
   | Logout      | /api/auth/logout | DELETE |

3. Talk

   | Description           | Route                                   | Method |
   |-----------------------|-----------------------------------------|--------|
   | Create a talk         | /api/talks/new                          | POST   |
   | Add member to a talk  | /api/talks/:talkId/add-member/:memberId | PUT    |
   | Get all talks *       | /api/talks/all                          | GET    |
   | Get talk by id        | /api/talks/:id                          | GET    |

4. Message

   | Description                                    | Route                      | Method |
   |------------------------------------------------|----------------------------|--------|
   | Send a message                                 | /api/messages/new          | POST   |
   | Find all messages (of user connected)          | /api/messages/all          | GET    |
   | Find all messages by talk (of user connected)* | /api/messages/all/talk/:id | GET    |

   (*) For this route you can add param to paginate result
        
      -> ex :/api/messages/all/talk/:id?page=2&limit=2