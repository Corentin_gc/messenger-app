const express = require('express')
const MessageModel = require("../model/message.model");
const {body, param, validationResult} = require("express-validator");
const UserModel = require("../model/user.model");
const TalkModel = require("../model/talk.model");
const router = express.Router()
const baseElementByPage = 2

/**
 * Create a message
 * @body {string} content - The content of this message
 */
router.post('/new',
    body('content').escape(),
    async (req, res) => {
        try {
            let talk = null
            // If user send a user and a talk
            if (req.body.recipient && req.body.talk) {
                return res.status(404).send({message: 'You have to specify a recipient or a talk, not both !'})
            }

            // If user not send user and talk
            if (!req.body.recipient && !req.body.talk) {
                return res.status(404).send({message: 'You have to specify a recipient or a talk!'})
            }

            // If user send a talk
            if (req.body.talk) {
                talk = await TalkModel.findOne({_id: req.body.talk})

                // If talk not found
                if (!talk) {
                    return res.status(404).send({message: 'Talk not found !'})
                }

                // If user request is include in talk
                if (!talk.members.includes(req.user._id)) {
                    return res.status(404).send({message: 'You are not member of this talk ! You can\'t send messages'})
                }
            }

            // If user send a other user
            if (req.body.recipient) {
                let recipient = await UserModel.findOne({_id: req.body.recipient})

                // If recipient not found
                if (!recipient) {
                    return res.status(404).send({message: 'Recipient not found !'})
                }

                // Find a task where user and recipient are in members and only two members
                talk = await TalkModel.findOne({members: {$all: [req.user._id, req.body.recipient], $size: 2}})

                // If talk not found
                if (!talk) {
                    talk = new TalkModel({
                        name: "New Talk",
                        author: req.user._id,
                        members: [
                            req.user._id,
                            req.body.recipient
                        ]
                    })
                    talk = await talk.save()
                }
            }

            // If talk is not null
            if (talk) {
                let message = new MessageModel(req.body)
                message.author = req.user._id
                message.talk = talk._id
                message = await message.save()
                talk.messages.push(message._id)
                talk = await talk.save()
                res.status(201).send({message: message})
            }
        } catch (e) {
            res.status(400).send(e)
        }
    })


/**
 * Find all messages
 */
router.get('/all',
    async (req, res) => {
    const messages = await MessageModel.find({author: req.user._id})
    res.send(messages)
})

/**
 * Find all messages by talk
 * @url-param {indexedDB} :id - id of talk
 * @query-param {int} page - The number of page
 * @query-param {int} limit - The limit of element for page
 */
router.get('/all/talk/:id',
    param('id')
        .notEmpty()
        .withMessage('Talk id is required !')
        .isMongoId()
        .withMessage('Talk id needs to be a mongodb id !'),
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({errors: errors.array()});
        }
        next()
    },
    async (req, res) => {
        let page = req.query.page
        let limit = req.query.limit
        let talkId = req.params.id
        let messages = null

        // If not page and not limit get all Talk
        if (!page && !limit){
            messages = await MessageModel.find({author: req.user._id, talk: talkId})
        }

        // If limit but not page return juste talk with limit
        if(limit && !page){
            messages = await MessageModel.find({author: req.user._id, talk: talkId}).limit(parseInt(limit))
        }

        // If page get only talk of this page (if not limit assign limit to baseElementByPage => 2)
        if(page){
            if (!limit){
                limit = baseElementByPage
            }

            messages = await MessageModel.find({author: req.user._id, talk: talkId}).skip((parseInt(page)-1)*parseInt(limit)).limit(parseInt(limit))
        }

        res.send({
            messages: messages,
            page: page,
            limit: limit,
            total: await MessageModel.count({author: req.user._id, talk: talkId})
        })
})

module.exports = router