const express = require('express')
const TalkModel = require("../model/talk.model");
const {param, validationResult, body} = require("express-validator");
const router = express.Router()
const baseElementByPage = 2

/**
 * Create a talk
 * @body {string} name - The name of talk
 */
router.post('/new',
    body('name').escape(),
    async (req, res) => {
        try {
            let talk = new TalkModel(req.body)
            talk.author = req.user._id
            talk.members.push(req.user._id)
            talk = await talk.save()
            res.status(201).send({talk: talk})
        } catch (e) {
            res.status(400).send(e)
        }
    })

/**
 * Find talks
 * @query-param {int} page - The number of page
 * @query-param {int} limit - The limit of element for page
 */
router.get('/all', async (req, res) => {
    let page = req.query.page
    let limit = req.query.limit
    let talks = null

    // If not page and not limit get all Talk
    if (!page && !limit){
        talks = await TalkModel.find({members: req.user._id})
    }

    // If limit but not page return juste talk with limit
    if(limit && !page){
        talks = await TalkModel.find({members: req.user._id}).limit(parseInt(limit))
    }

    // If page get only talk of this page (if not limit assign limit to baseElementByPage => 2)
    if(page){
        if (!limit){
            limit = baseElementByPage
        }

        talks = await TalkModel.find({members: req.user._id}).skip((parseInt(page)-1)*parseInt(limit)).limit(parseInt(limit))
    }

    res.send({
        talks: talks,
        page: page,
        limit: limit,
        total: await TalkModel.count({members: req.user._id})
    })
})

/**
 * Find by id
 * @url-param {indexedDB} :id - Id of talk
 */
router.get('/:id',
    param('id')
        .notEmpty()
        .withMessage('Id is required !')
        .isMongoId()
        .withMessage('Id needs to be a mongodb id !'),
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({errors: errors.array()});
        }
        next()
    },
    async (req, res) => {
        const talk = await TalkModel.findOne({_id: req.params.id})
        if (!talk) {
            res.status(404).send({message: 'Talk not found !'})
        }
        res.send({talk})
    })

/**
 * Add member to talk
 * @url-param {indexedDB} :talkId - id of talk
 * @url-param {indexedDB} :memberId - id of member
 */
router.put('/:talkId/add-member/:memberId',
    param('talkId')
        .notEmpty()
        .withMessage('talkId is required !')
        .isMongoId()
        .withMessage('talkId needs to be a mongodb id !'),
    param('memberId')
        .notEmpty()
        .withMessage('memberId is required !')
        .isMongoId()
        .withMessage('memberId needs to be a mongodb id !'),
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({errors: errors.array()});
        }
        next()
    },
    async (req, res) => {
        let talk = await TalkModel.findOne({_id: req.params.talkId});

        if (!talk) {
            return res.status(404).send({message: 'Talk not found !'})
        }

        if (!talk.author === req.user._id) {
            return res.status(404).send({message: 'Your are not allowed to do this action !'})
        }

        if (talk.members.includes(req.params.memberId)) {
            return res.status(404).send({message: 'This user is already add in members !'})
        }

        try {
            talk.members.push(req.params.memberId)
            talk = await talk.save()
            res.status(201).send({talk: talk})
        } catch (e) {
            res.status(400).send(e)
        }
    })

module.exports = router